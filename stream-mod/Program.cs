﻿using GTA;
using System;
using System.Drawing;
using System.Xml;
using System.IO;
using System.Text;
using WMPLib;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Timers;

namespace GTAV_InternetRadioStreamer
{
    class Main : Script
    {
        // Author: Xenius | 2015.12.12. //
        public static int ActualStation = 0;
        public static int AllStations = 0;
        public static int ActualVolume = 20;
        public static bool isRadioPlayerEnabled = false;
        public static bool isMP3PlayerEnabled = false;
        public static List<string> stations = new List<string>();
        public static string playerStatusText;
        public static WindowsMediaPlayer wmp = new WindowsMediaPlayer();
        public static System.Timers.Timer _timer = new System.Timers.Timer(2000);
        public static int songCount = 0;
        
        public static void showPlayerStatus(string text)
        {
            playerStatusText = text;
            if (_timer.Enabled)
                _timer.Stop();
            _timer.Elapsed += new ElapsedEventHandler(hidePlayerStatus);
            _timer.Enabled = true;
        }

        static void hidePlayerStatus(object sender, ElapsedEventArgs e)
        {
            playerStatusText = null;
        }

        public static void initRadioPlayer()
        {
            wmp.controls.stop();
            if (File.Exists("scripts/stations.xml"))
            {
                XmlTextReader textReader = new XmlTextReader("scripts/stations.xml");
                textReader.Read();
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(textReader);

                XmlNodeList RadioURL = xmlDoc.GetElementsByTagName("station");
                stations.Add("off");
                for (int i = 0; i < RadioURL.Count; i++)
                {
                    stations.Add(RadioURL[i].InnerText);
                }

                AllStations = RadioURL.Count;
                showPlayerStatus("Stations loaded: " + AllStations + " | Switch: LEFT/RIGHT Arrows | Volume: PgUP, PgDown");
            }
            else {
                showPlayerStatus("Error: stations.xml not found!");
            }
        }
        
        public static void initMP3Player()
        {
            wmp.controls.stop();
            songCount = 0;
            DirectoryInfo dir = new DirectoryInfo(@"scripts/mp3");
            FileInfo[] files = dir.GetFiles();
            IWMPPlaylist playlist = wmp.playlistCollection.newPlaylist("gtavplaylist");
            foreach (FileInfo file in files)
            {
                if (file.Extension == ".mp3" || file.Extension == ".mP3" || file.Extension == ".MP3" || file.Extension == ".wav" || file.Extension == ".ogg" || file.Extension == ".Mp3")
                {
                    IWMPMedia media;
                    media = wmp.newMedia(file.FullName);
                    playlist.appendItem(media);
                    songCount++;
                }
            }
            wmp.currentPlaylist = playlist;
            wmp.controls.play();

            showPlayerStatus("Songs loaded: " + songCount + " | Switch: LEFT/RIGHT Arrows | Volume: PgUP, PgDown");
        }

        public Main()
        {
            bool folderExists = Directory.Exists("scripts/mp3");
            if (!folderExists)
                Directory.CreateDirectory("scripts/mp3");

            Tick += OnTick;
            KeyDown += OnKeyDown;

            showPlayerStatus("Radio Stream v1.0 loaded | Author: Xenius");

            wmp.settings.volume = ActualVolume;
            wmp.MediaError += playerError;
            wmp.MediaChange += playerMediaChange;
            wmp.Buffering += playerBuffering;
            wmp.settings.setMode("shuffle", true);
        }

        public void OnKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                    if (!isRadioPlayerEnabled && !isMP3PlayerEnabled)
                    {
                        isRadioPlayerEnabled = true;
                        isMP3PlayerEnabled = false;
                        initRadioPlayer();
                    }
                    else if(isRadioPlayerEnabled && !isMP3PlayerEnabled)
                    {
                        isRadioPlayerEnabled = false;
                        isMP3PlayerEnabled = true;
                        initMP3Player();
                    }
                    else
                    {
                        isRadioPlayerEnabled = false;
                        isMP3PlayerEnabled = false;
                        wmp.controls.stop();
                        showPlayerStatus("Radio mode: OFF");
                    }
                    break;
                case Keys.Right:
                    if (isRadioPlayerEnabled)
                    {
                        if (ActualStation < AllStations)
                        {
                            ActualStation++;
                            showPlayerStatus("Station: " + ActualStation);
                            wmp.URL = stations[ActualStation];
                            wmp.controls.play();
                        }
                        else
                        {
                            ActualStation = 0;
                            wmp.controls.stop();
                            showPlayerStatus("Radio turned off.");
                        }
                    } else if (isMP3PlayerEnabled)
                    {
                        wmp.controls.next();
                        wmp.controls.play();
                    }
                    break;
                case Keys.Left:
                    if (isRadioPlayerEnabled)
                    {
                        if (ActualStation > 1)
                        {
                            ActualStation--;
                            showPlayerStatus("Station: " + ActualStation);
                            wmp.URL = stations[ActualStation];
                            wmp.controls.play();
                        }
                        else if (ActualStation > 0)
                        {
                            ActualStation = 0;
                            wmp.controls.stop();
                            showPlayerStatus("Radio turned off.");
                        }
                        else
                        {
                            ActualStation = AllStations;
                            showPlayerStatus("Station: " + ActualStation);
                            wmp.URL = stations[ActualStation];
                            wmp.controls.play();
                        }
                    }
                    else if (isMP3PlayerEnabled)
                    {
                        wmp.controls.previous();
                        wmp.controls.play();
                    }
                    break;
                case Keys.PageDown:
                    if (ActualVolume > 0)
                    {
                        ActualVolume = ActualVolume - 1;
                        wmp.settings.volume = ActualVolume;
                    }
                    showPlayerStatus("Volume: " + ActualVolume);
                    break;
                case Keys.PageUp:
                    if (ActualVolume < 100)
                    {
                        ActualVolume = ActualVolume + 1;
                        wmp.settings.volume = ActualVolume;
                    }
                    showPlayerStatus("Volume: " + ActualVolume);
                    break;
                default:
                    break;
            }
        }

        public void OnTick(object sender, EventArgs e)
        {
            if (playerStatusText != null)
                new UIText(playerStatusText, new Point(5, 5), 0.4f, Color.White, GTA.Font.ChaletLondon, false).Draw();
        }

        static void playerError(object pMediaObject)
        {
            if (ActualStation != 0)
                showPlayerStatus("Stream error.");
        }

        static void playerMediaChange(object pMediaObject)
        {
            showPlayerStatus(wmp.controls.currentItem.name);
        }

        public void playerBuffering(bool started)
        {
            if (started)
                showPlayerStatus("Buffering ...");
        }
    }
}